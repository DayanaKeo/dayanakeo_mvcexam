<?php
namespace App\Controller;

use App\Model\ContactModel;
use App\Service\Form;
use Core\Kernel\AbstractController;
use App\Service\Validation;
//use App\Model\ContactModel;
//use Core\Kernel\AbstractModel;

class ContactController extends AbstractController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index() {
        $this->render('app.contact.index', array(
            'contacts' => ContactModel::all(),
        ));
    }

    public function single($id)
    {
        $contact = $this->getContactByIdOr404($id);
        $this->render('app.contact.single', array(
            'contact'=> $contact,
        ));
    }

    public function add() {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            // Faille XSS.
            $post = $this->cleanXss($_POST);
            // Validation
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                ContactModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Votre message a été envoyé avec succes !');
                // redirection
                $this->redirect('contact');
            }
        }
        $form = new Form($errors);
        $this->render('app.contact.add', array(
            'form' => $form,
        ));
    }

    private function getContactByIdOr404($id)
    {
        $contact = ContactModel::findById($id);
        if(empty($contact)) {
            $this->Abort404();
        }
        return $contact;
    }


    private function validate($v,$post)
    {
        $errors = [];
        $errors['sujet'] = $v->textValid($post['sujet'], 'sujet',5, 150);
        $errors['email'] = $v->textValid($post['email'], 'email',5, 150);
        $errors['message'] = $v->textValid($post['message'], 'message');
        return $errors;
    }


}