<?php
namespace App\Model;
use Core\Kernel\AbstractModel;
use Core\App;

class ContactModel extends AbstractModel
{
    protected static $table = 'contact';

    protected $id;
    protected $sujet;
    protected $email;
    protected $message;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (sujet, email, message, created_at) VALUES (?,?,?, NOW())",
            array($post['sujet'], $post['email'], $post['message'])
        );
    }
}