<section id="formulaire" class="wrap">
    <form action="" method="post" novalidate>
        <?php echo $form->label('sujet'); ?>
        <?php echo $form->input('sujet', 'text', $contact->sujet ?? ''); ?>
        <?php echo $form->error('sujet'); ?>

        <?php echo $form->label('email'); ?>
        <?php echo $form->input('email', 'email', $contact->email ?? ''); ?>
        <?php echo $form->error('email'); ?>

        <?php echo $form->label('message'); ?>
        <?php echo $form->input('message', $contact->message ?? ''); ?>

        <?php echo $form->submit('submitted', 'envoyer'); ?>
    </form>
</section>
<!-- /#formulaire -->